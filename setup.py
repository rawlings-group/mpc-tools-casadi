"""See README for description."""
from setuptools import setup

setup(
   name="MPCTools",
   version="2.4.2",
   description="Nonlinear MPC tools for use with CasADi",
   author="Michael Risbeck", 
   author_email="risbeck@wisc.edu",
   url="https://bitbucket.org/rawlings-group/mpc-tools-casadi",
   long_description=__doc__,
   packages=["mpctools"],
   install_requires=["casadi"],
   python_requires=">=3.6",
   license="GNU LGPLv3",
)

